#!/usr/bin/python
import os, sys, psutil, re, logging, subprocess, timeit
from datetime import datetime
logger = logging.getLogger('ha')
logger.info("Library os_stats.py started...")

def get_usage(value):
	logger.debug("Function get_usage(%s) started..." % value)
	if value == 'cpu':
		av = psutil.cpu_percent()
		logger.debug("Function get_usage(%s) finished, return CPU average %s." % (value, av))
		return av
	elif value == 'uptime':
		with open('/proc/uptime', 'r') as f:
			uptime = float(f.readline().split()[0])
		logger.debug("Function get_usage(%s) finished, return uptime=%s." % (value, uptime))
		return uptime
	elif value == 'mem':
		usage = psutil.virtual_memory()
		logger.debug("Function get_usage(%s) finished, return usage.used=%s, usage.percent=%s." % (value, usage.used, usage.percent))
		return int(usage.used), int(usage.percent)
	elif value == 'disk':
		usage = psutil.disk_usage('/')
		logger.debug("Function get_usage(%s) finished, return usage.used=%s, usage.percent=%s." % (value, usage.used, usage.percent))
		return int(usage.used), int(usage.percent)
		# Function that reads raspberry temperature sensor
	elif value == 'os_temp':
		temp = re.findall('\d+',os.popen('axp209 --temperature').readline())[:1]
		logger.debug("Function get_usage(%s) finished, return os_temp=%s." % (value, temp))
		return float(temp[0])
	elif value == 'network':
		stat = psutil.net_io_counters(pernic=True)['eth0']
		tx = stat.bytes_sent
		rx = stat.bytes_recv
		logger.debug("Function get_usage(%s) finished, return tx=%s, rx=%s." % (value, tx, rx))
		return int(tx), int(rx)
	elif value == 'battery':
		battery_per = os.popen("bash /root/publisher/python/battery.sh|  grep -i 'gauge' | cut -c16-20").read().strip().replace('%','')
		battery_vol = os.popen("bash /root/publisher/python/battery.sh|  grep -i 'volt' |  cut -c18-27").read().strip().replace('mV','')
		battery_temp = os.popen("bash /root/publisher/python/battery.sh|  grep -i 'temp' |  cut -c23-28").read().strip().replace('c','')
		if int(battery_per) > 100 or battery_per is None:
			battery_per = 'N/A'
			battery_vol = 'N/A'
			logger.debug("Function get_usage(%s) finished, return battery_per=%s, battery_ma=%s, battery_temp=%s." % (value, battery_per, battery_vol, battery_temp))
			return battery_per, battery_vol, battery_temp
		else:
			return float(battery_per), float(battery_vol), float(battery_temp)

def bytes2human(n):
	logger.debug("Function bytes2human(%s) started..." % n)
	symbols = ('K', 'M', 'G', 'T', 'P', 'E', 'Z', 'Y')
	prefix = {}
	for i, s in enumerate(symbols):
		prefix[s] = 1 << (i+1)*10
	for s in reversed(symbols):
		if n >= prefix[s]:
			value = int(float(n) / prefix[s])
			return '%s%s' % (value, s)
	return "%sB" % n

def wifi(op):
	logger.debug("Function wifi(%s) started..." % op)
	import netifaces
	if op == 'essid':
		essid = os.popen('iwgetid -r').read().strip()
		logger.debug("Function wifi(%s) finished, return essid=%s." % (op, essid))
		return essid
	# Find out the link quality
	elif op == 'link':
		link = re.findall('\d+',os.popen('iwconfig wlan0 | grep level').readline())[:3]
		link = int(link[2])*-1
		logger.debug("Function wifi(%s) finished, return link=%s." % (op, link))
		return link
	# Find out our IP
	elif op == 'ip':
		ip = os.popen("ifconfig wlan0 | grep 'inet\ addr' | cut -d: -f2 | cut -d' ' -f1").read().strip()
		logger.debug("Function wifi(%s) finished, return ip=%s." % (op, ip))
		return ip
	elif op == 'mac':
		mac = os.popen("ifconfig wlan0 | grep HWaddr |cut -dH -f2|cut -d\  -f2").read().strip()
		logger.debug("Function wifi(%s) finished, return mac=%s." % (op, mac))
		return mac
	# Return N/A if Wifi device not present
	else:
		logger.debug("Function wifi(%s) finished but something went wrong. Returning 'N/A'." % op)
		return "N/A"

if __name__ == "__main__":
	print("This module is executable, please run publisher.py instead")
