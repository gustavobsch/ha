class HTTPRequestHandler(BaseHTTPRequestHandler):
	def do_GET(self):
		if None != re.search('/api/v1/getrecord/*', self.path):
			recordID = self.path.split('/')[-1]
		if LocalData.records.has_key(recordID):
			self.send_response(200)
			self.send_header('Content-Type', 
'application/json')
			self.end_headers()
			self.wfile.write(LocalData.records[recordID])
		else:
			self.send_response(403)
			self.send_header('Content-Type', 
'application/json')
			self.end_headers()
		return
    
class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
	allow_reuse_address = True
 
	def shutdown(self):
		self.socket.close()
		HTTPServer.shutdown(self)
    
class http_server():
	def __init__(self, ip, port):
		print "ip is type %s %s" % (ip, type(ip))
		self.server = ThreadedHTTPServer((ip, port), 
HTTPRequestHandler)
 
	def start(self):
		self.server_thread = 
threading.Thread(target=self.server.serve_forever)
		self.server_thread.daemon = True
		self.server_thread.start()
 
	def waitForThread(self):
		self.server_thread.join()
 
	def stop(self):
		self.server.shutdown()
		self.waitForThread()

