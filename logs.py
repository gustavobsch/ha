#!/usr/bin/python
import logging, logging.handlers
def setup_logger(name, runmode):
	formatter = logging.Formatter('%(asctime)s-%(levelname)s-%(module)s-%(threadName)s-%(message)s')
	handler = logging.FileHandler('/var/log/ha.log')
	handler.setFormatter(formatter)
	logger = logging.getLogger(name)
	# When running in test mode we run with increased log level verbosity 
	if runmode == 'debug':
		logger.setLevel(logging.INFO)
	elif runmode == 'normal':
		logger.setLevel(logging.WARNING)
		#logger.setLevel(logging.DEBUG)
		#logger.setLevel(logging.INFO)
	logger.addHandler(handler)
	logger.info("Library logs.py started...")
	return logger

def setup_elogger(name):
	HOST = 'smtp.noip.us'
	FROM = '"Home Artificial Intelligence Assistant" <ha@noip.us>'
	TO = 'gustavobsch@outlook.com'
	SUBJECT = 'Critical Event From ha.py'
	formatter = logging.Formatter(fmt='%(asctime)s-%(hostname)s-%(levelname)s-%(module)s-%(threadName)-10s-%(message)s')
	handler = logging.handlers.SMTPHandler(HOST, FROM, TO, SUBJECT)
	handler.setFormatter(formatter)
	elogger = logging.getLogger(name)
	elogger.addHandler(handler)
	elogger.setLevel(logging.CRITICAL) 
	return elogger

if __name__ == "__main__":
	print("This module is executable, please run ha.py instead")
