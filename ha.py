#!/usr/bin/python
from datetime import datetime
import time, os, sys, traceback, logging, threading, logs, json, timeit, ast, utils, random, pytz
import paho.mqtt.client as mqtt
from BaseHTTPServer import BaseHTTPRequestHandler,HTTPServer
from SocketServer import ThreadingMixIn
from json2html import *

### Set global variables
global start

### Get run mode

if len(sys.argv) == 2:
	runmode = sys.argv[1]
	submode = 'normal'
elif len(sys.argv) == 3:
	runmode = sys.argv[1]
	submode = sys.argv[2]
else:
	runmode = 'normal'
	submode = 'normal'

### Start timers and logging
starttime=time.time()
start = timeit.default_timer()
start_stats = datetime.now()
logger = logs.setup_logger('ha', runmode)
elogger = logs.setup_elogger('email')
logger.warning("Program Started...")

### Load config parameters
def load_config():
	import ConfigParser
	global config
	config = {}
	config_file = ConfigParser.RawConfigParser()
	config_file.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), '', 'config.cfg'))
	config.update({
	'hostid':config_file.get('host', 'id'), 
	'mqtt_broker_hostname':config_file.get('mqtt_broker', 'hostname'),
	'mqtt_broker_port':config_file.getint('mqtt_broker', 'port'),
	'mqtt_broker_username':config_file.get('mqtt_broker', 'username'),
	'mqtt_broker_pass':config_file.get('mqtt_broker', 'password'),
	'mqtt_publish_format':config_file.get('mqtt_broker', 'format'),
	'write_stats_interval':config_file.getint('timers', 'write_stats_interval'),
	'supervisor_interval':config_file.getint('timers', 'supervisor'),
	'tsa':config_file.getint('timers', 'tsa'),
	'etsa':config_file.getint('timers', 'etsa'),
	'mqtt_topic':config_file.get('mqtt_broker', 'topic'),
	'hue_bridge_hostname':config_file.get('hue', 'bridge'),
	'hue_bridge_port':config_file.get('hue', 'port'),
	'hue_transitions':config_file.get('hue', 'transitions'),
	'http_ip':config_file.get('http', 'bind_address'),
	'http_port':config_file.getint('http', 'port'),
	'indoor_locations':config_file.get('locations', 'indoors').split(','),
	'outdoor_locations':config_file.get('locations', 'outdoors').split(','),
	'city':config_file.get('environment', 'location'),
	'timezone':config_file.get('environment', 'timezone'),
	'openweathermap_city':config_file.getint('openweathermap', 'city'),
	'openweathermap_appid':config_file.get('openweathermap', 'appid')	
	})

### Load enviromental variables
def load_environment():
	global home
	global in_lux
	global out_lux
	global in_temp
	global out_temp
	global in_hum
	global out_hum
	global base_payload
	global payload
	global localtime
	now = datetime.now(pytz.timezone(config['timezone'])).strftime('%Y-%m-%d %H:%M:%S')
	home = {
	'indoors':
		{'overall':
			{'light':
				{'ambient_light':'medium', 'lux': 0},
			'temperature':15},
		'locations':{}}, 
	'outdoors':
		{'overall':
			{'light':
				{'ambient_light':'low', 'lux': 0},
			'temperature':15},
		'locations':{}},
	'environment':
		{'localtime':'n/a'}
	}
	for location in config['outdoor_locations']:
		home['outdoors']['locations'].update({location:{
		'ambient_light':'low',
		'sensors':{},
		'presence':{'timestamp':now,
		'score':0,
		'asserted':False,
		'extended':{'score':0,'timestamp':now,'asserted':False}}
		}})
	for location in config['indoor_locations']:
		home['indoors']['locations'].update({location:{
		'ambient_light':'low',
		'sensors':{},
		'presence':{'timestamp':now,
		'score':0,
		'asserted':False,
		'extended':{'score':0,'timestamp':now,'asserted':False}}
		}})
	in_lux = utils.ringlist(10)
	out_lux = utils.ringlist(10)
	in_temp = utils.ringlist(10)
	out_temp = utils.ringlist(10)
	in_hum = utils.ringlist(10)
	out_hum = utils.ringlist(10)
	

### Learn what smart things are out there from MQTT messages
class the_learner(threading.Thread):
	def __init__(self, category=None, kwargs=None):
		threading.Thread.__init__(self)
		self.message = kwargs
		self.category = category
		self.localtime = datetime.now(pytz.timezone(config['timezone']))
		return
	def run(self):
		try:
			if self.category == 'thing':
				logger.debug("The Learner received the following from the MQTT thread '%s'" % self.message)
				for key, value in self.message.iteritems():
					if key == 'sublocation':
						sublocation = value
						if sublocation in config['outdoor_locations']:
							inout = 'outdoors'
						else:
							inout = 'indoors'
					elif key == 'gpios':
						sensors = value
						gpios = {}
						logger.info("Updating location '%s' with '%s'." % (sublocation, sensors))
						for gpio, sensor in sensors.iteritems():
							gpios.update({sensor:False})
						gpios = {sublocation: {key: gpios}}
				for key, value in gpios.iteritems():
					home[inout]['locations'][sublocation].update(value)
				logger.info("Learning completed, updated smart things database for this location: '%s'. Thread ends here." % home[inout]['locations'][sublocation])
			# Learn enviroment starts here
			elif self.category == 'environment':
				logger.info("Thee Learner received the following from the MQTT thread '%s'" % self.message)
				for key, value in self.message.iteritems():
					## conditional flagged, runs everytime sublocation exists
					if key == 'sublocation':
						sublocation = value
						if sublocation in config['outdoor_locations']:
							inout = 'outdoors'
						else:
							inout = 'indoors'
						for key, value in self.message.iteritems():
							if key == 'illuminance':
								readout = value
								readouts = {}
								for sensor, value in readout.iteritems():
									if sensor in ['c','color_temp','light', 'uv', 'uvi', 'lux', 'ir', 'vis']:
										# for now just check lux value; anything above 1000 c color means is day time
										if inout == 'outdoors':
											if sensor == 'lux':
												if value > 5000:
													home[inout]['locations'][sublocation].update({'ambient_light':'high'})
												elif value <= 5000 and value >= 250:
													home[inout]['locations'][sublocation].update({'ambient_light':'medium'})
												else:
													home[inout]['locations'][sublocation].update({'ambient_light':'low'})
												out_lux.append(value)
												average = int(sum(out_lux.get()) / float(len(out_lux.get())))
												home[inout]['overall']['light'].update({'lux':average})
												if average > 5000:
													home[inout]['overall']['light'].update({'ambient_light':'high'})
												elif average <= 5000 and average > 300:
													home[inout]['overall']['light'].update({'ambient_light':'medium'})
												else:
													home[inout]['overall']['light'].update({'ambient_light':'low'})
										elif inout == 'indoors':
											if sensor == 'lux':
												if value > 300:
													home[inout]['locations'][sublocation].update({'ambient_light':'high'})
												elif value <= 300 and value >= 20:
													home[inout]['locations'][sublocation].update({'ambient_light':'medium'})
												else:
													home[inout]['locations'][sublocation].update({'ambient_light':'low'})
												in_lux.append(value)
												average = int(sum(in_lux.get()) / float(len(in_lux.get())))
												home['indoors']['overall']['light'].update({'lux':average})
												#print in_lux.get()
												#print average
												if average > 300:
													home[inout]['overall']['light'].update({'ambient_light':'high'})
												elif average <= 300 and average >= 20:
													home[inout]['overall']['light'].update({'ambient_light':'medium'})
												else:
													home[inout]['overall']['light'].update({'ambient_light':'low'})
										if sensor == 'lux':
											readouts.update({sensor:value})
									if sublocation in config['indoor_locations'] or sublocation in config['outdoor_locations']:
										home[inout]['locations'][sublocation]['sensors'].update(readouts)
							elif key == 'climate':
								# update indoors temperature condition, lets inspect messasge again
								readout = value
								readouts = {}
								for sensor, value in readout.iteritems():
									if inout == 'outdoors':
										if sensor == 'temperature':
											out_temp.append(value)
											average = int(sum(out_temp.get()) / float(len(out_temp.get())))
											#print "out temp: %s average %s" % (out_temp.get(), average)
											home[inout]['overall'].update({'temperature':average})
											
										elif sensor == 'humidity':
											out_hum.append(value)
											average = int(sum(out_hum.get()) / float(len(out_hum.get())))
											home[inout]['overall'].update({'humidity':average})
										elif sensor == 'rain_volt':
											if value <= 2.4:
												home[inout]['overall'].update({'rain':True})
											else:
												home[inout]['overall'].update({'rain':False})
									elif inout == 'indoors':
										if sensor == 'temperature':
											in_temp.append(value)
											average = int(sum(in_temp.get()) / float(len(in_temp.get())))
											#print "in temp: %s average %s" % (in_temp.get(), average)
											# update the indoors average 
											home[inout]['overall'].update({'temperature':average})
										elif sensor == 'humidity':
											in_hum.append(value)
											average = int(sum(in_hum.get()) / float(len(in_hum.get())))
											home[inout]['overall'].update({'humidity':average})
									if sensor == 'pressure':
										home['outdoors']['overall'].update({'pressure':value})
									elif sensor in ['temperature', 'humidity', 'rain_volt']:
										readouts.update({sensor:value})
								if sublocation in config['indoor_locations'] or sublocation in config['outdoor_locations']:
									home[inout]['locations'][sublocation]['sensors'].update(readouts)
							elif key == 'air':
								readout = value
								readouts = {}
								# update indoors air conditions, lets inspect messasge again
								for sensor, value in readout.iteritems():
									if sensor in ['co2', 'dust']:
										# get these two air quality indicators
										readouts.update({sensor:value})
								home[inout]['locations'][sublocation]['sensors'].update(readouts)
			elif self.category == 'presence':
				logger.debug("The Learner received the following from the MQTT thread '%s'" % self.message)
				for key, value in self.message.iteritems():
					if key == 'sublocation':
						sublocation = value
						if sublocation in config['outdoor_locations']:
							inout = 'outdoors'
						else:
							inout = 'indoors'
					elif key == 'deviceid':
						deviceid = value
					# PIR event means 100 % presence score, the rest only hold 20
					elif value == 'pir':
						ps = 100
					elif value == 'mic':
						ps = 20
					elif value == 'vib':
						ps = 20
					# here we put a timestamp to the presence event
					home[inout]['locations'][sublocation]['presence']['timestamp'] = self.localtime.strftime('%Y-%m-%d %H:%M:%S')
				# Extended presence score is maintained below
				epa = home[inout]['locations'][sublocation]['presence']['extended']['asserted']
				ocps = home[inout]['locations'][sublocation]['presence']['extended']['score']
				ncps = ocps + ps
				if ncps >= 400 and epa is False:
					print "Location '%s' has upgraded to extended presence status." % sublocation
					home[inout]['locations'][sublocation]['presence']['extended']['score'] = ncps
					home[inout]['locations'][sublocation]['presence']['extended']['asserted'] = True
					home[inout]['locations'][sublocation]['presence']['extended']['timestamp'] = self.localtime.strftime('%Y-%m-%d %H:%M:%S')
				elif ncps <= 1000 and epa is True:
					home[inout]['locations'][sublocation]['presence']['extended']['score'] = ncps
					home[inout]['locations'][sublocation]['presence']['extended']['timestamp'] = self.localtime.strftime('%Y-%m-%d %H:%M:%S')
				elif ncps < 400:
					home[inout]['locations'][sublocation]['presence']['extended']['score'] = ncps
				# Presence score maintained below
				ops = home[inout]['locations'][sublocation]['presence']['score'] 
				nps = home[inout]['locations'][sublocation]['presence']['score'] + ps
				logger.info("Received a '%s' point presence event from location '%s'. Upgrading presence score from '%s' to '%s'" % (ps, sublocation, ops, nps)),
				logger.info("and extended presence score from '%s' to '%s'." % (ocps, ncps))
				# When presence score is 100 and, therefore, presence for a location has been asserted, we invoke the decider to take it from there
				if nps >= 100:
					home[inout]['locations'][sublocation]['presence']['asserted'] = True
					home[inout]['locations'][sublocation]['presence']['score'] = 100
					event = {'presence':{'sublocation':sublocation,'asserted':True,'deviceid':deviceid}}
					thread_name = str(sublocation+"_presence")
					decider  = the_decider(name=thread_name, args=(1,), kwargs=event)
					decider.start()
				elif nps >= 60 and nps < 100:
					home[inout]['locations'][sublocation]['presence']['asserted'] = True
					home[inout]['locations'][sublocation]['presence']['score'] = nps
					event = {'presence':{'sublocation':sublocation,'asserted':True,'deviceid':deviceid}}
					thread_name = str(sublocation+"_presence")
					decider  = the_decider(name=thread_name, args=(1,), kwargs=event)
					decider.start()
				elif nps < 60:
					home[inout]['locations'][sublocation]['presence']['score'] = nps
					
			logger.info("Learning completed, enviromental conditions database: '%s'. Thread ends here." % home)
		except Exception:
			sys.excepthook(*sys.exc_info())

class the_decider(threading.Thread):
	def __init__(self, group=None, target=None, name=None, args=(), kwargs=None, verbose=None):
		threading.Thread.__init__(self, group=group, target=target, name=name, verbose=verbose)
		self.args = args
		self.kwargs = kwargs
		self.twilight = utils.get_twilight(config['city'])
		self.ttl = 0
		self.time = time.localtime().tm_hour
		return
#	def add_time(self):
#		if self.ttl < 40 and home['indoors']['overall']['light']['ambient_light'] == 'low':
#			self.ttl = self.ttl + 60
#			#logger.info("%s New time to live: %s." % (self.name, self.ttl))
#			print "Another presence event for this location"
#
	def run(self):
		try:
			logger.debug("The Decider received the following event '%s'" % self.kwargs)
			event = sensor='n/a'
			for key, value in self.kwargs.iteritems():
				if key == 'sublocation':
					sublocation = value
					if sublocation in config['outdoor_locations']:
							inout = 'outdoors'
					else:
						inout = 'indoors'
				elif key == 'sensor':
					sensor = value
				elif key == 'presence':
					event = 'presence'
			# With the what where and who we being desission process
			if event == 'presence':
				sublocation = value['sublocation']
				deviceid = value['deviceid']
				if sublocation in config['outdoor_locations']:
						inout = 'outdoors'
				else:
					inout = 'indoors'
				ambient_light = home[inout]['locations'][sublocation]['ambient_light']
				presence = home[inout]['locations'][sublocation]['presence']['asserted']
				print("Presence event received from '%s', looking for smart things in that location..." % sublocation)
				# Check if there's a hue light group in this location
				if sublocation not in home[inout]['locations']:
					logger.info("Location '%s' is not present in our database, thread ends here." % sublocation)
					return
				logger.info("State maintaned for this location:'%s'" % home[inout]['locations'][sublocation])
				for key, value in home[inout]['locations'][sublocation].iteritems():
					# for presence events we turn lights on only when light conditions are low in that location
					if key == 'hue_lights':
						print "Presence in '%s' is now" % sublocation,
						for light in value:
							if presence is True and ambient_light == 'low':
								print "'%s' and ambient_light is '%s', turning light '%s' on." % (presence, ambient_light, light)
								status = home[inout]['locations'][sublocation]['hue_lights'][int(light)]
								if status:
									print("Light '%s' is already on." % light)
									continue
								print("Light '%s' in '%s' was off, turning the group on." % (light, sublocation))
								# low bright red light from midnight to 5 am
								if home['environment']['time_of_day'] == 'midnight':
									bright = 1
									xy = [0.72,0.25]
								# full birght during evening or morning when light conditions are low
								elif home[inout]['locations'][sublocation]['ambient_light'] == 'low' and home['environment']['time_of_day'] in ['evening', 'morning', 'afternoon']:
									bright = 254
									xy = [0.5,0.4]
								# mid light late evening from 22 to 24
								elif home['environment']['time_of_day'] == 'night':
									bright = 127
									xy = [0.5,0.4]
								# whatever time we are not capturing above lets turn lights green for debugging porpuses
								else:
									bright = 254
									xy = [0.3,0.7]
								command = {'transitiontime' : 1, 'on' : True, 'bri' : bright, 'xy': xy}
								for light in home[inout]['locations'][sublocation]['hue_lights']:
									home[inout]['locations'][sublocation]['hue_lights'][int(light)] = True
								hue.set_light(value, command)
							elif presence is False:
								status = home[inout]['locations'][sublocation]['hue_lights'][int(light)]
								if status:
									print "'%s' and light '%s' status is '%s', turning light '%s' off." % (presence, light, status, light)
									command =  {'transitiontime' : 10, 'on' : False}
									hue.set_light(value, command)
									home[inout]['locations'][sublocation]['hue_lights'][int(light)] = False
					elif key == 'gpios':
						if len(value) == 0:
							logger.info("Location '%s' has no smart GPIO's available, thread ends here." % sublocation)
							return
						for gpio in value:
							if gpio == 'relay' and inout == 'outdoors':
								logger.info("Found a relay activated light in '%s', turning it on." % sublocation)
								# midnight to five am warrants flashing light and warning beep
								if home['environment']['time_of_day'] == 'midnight':
									activate(gpio, 'warn', sublocation, deviceid)
									home[inout]['locations'][sublocation]['gpios'][gpio] = True
									time.sleep(0.5)
									activate(gpio, 'on', sublocation, deviceid)
									self.ttl = 360
									loop = 1
									while loop == 1:
										if self.ttl % 5 == 0:
											logger.info("Light relay in %s has '%s' secs to live..." % (sublocation, self.ttl))
										self.ttl = self.ttl - 1
										if self.ttl <= 0:
											activate(gpio, 'off', sublocation, deviceid)
											home[inout]['locations'][sublocation]['gpios'][gpio] = False
											logger.info("Light relay in '%s' is now off, thread ends here." % sublocation)
											loop = 0
										time.sleep(1)
								elif home['outdoors']['overall']['light']['ambient_light'] == 'low':
									# turn light for ten secs
									activate(gpio, 'on', sublocation, deviceid)
									home[inout]['locations'][sublocation]['gpios'][gpio] = True
									self.ttl = 30
									loop = 1
									while loop == 1:
										if self.ttl % 5 == 0:
											logger.info("Light relay in %s has '%s' secs to live..." % (sublocation, self.ttl))
										self.ttl = self.ttl - 1
										if self.ttl <= 0:
											activate(gpio, 'off', sublocation, deviceid)
											home[inout]['locations'][sublocation]['gpios'][gpio] = False
											logger.info("Light relay in '%s' is now off, thread ends here." % sublocation)
											loop = 0
										time.sleep(1)
							elif gpio == 'buz' and inout == 'outdoors':
								# midnight to 5 am warrants flashing light warning beep every time we detet movement
								if home['environment']['time_of_day'] == 'midnight':
									mode = 'midnight'
								elif inout == 'outdoors':
									mode = 'warn'
								activate(gpio, mode, sublocation, deviceid)
							elif gpio in['buz'] and inout == 'indoors':
								mode = 'feedback'
								activate(gpio, mode, sublocation, deviceid)
							elif gpio in['led'] and inout == 'indoors':
								mode = 'blink'
								activate(gpio, mode, sublocation, deviceid)
			return
		except Exception:
			sys.excepthook(*sys.exc_info())

### This thread maintains envriomental variables at time intervals
class the_supervisor(threading.Thread):
	def __init__(self, interval):
		threading.Thread.__init__(self)
		self.interval = interval
		self.name = 'the_supervisor'
		return
	def run(self):
		import yaml
		logger.info("Refreshing enviromental variables at '%s' secs intervals..." % self.interval)
		while True:
			try:
				## update weather first
				logger.info("Refreshing weather from openweathermap.")
				weather = utils.get_weather(config['openweathermap_city'], config['openweathermap_appid'])
				home['environment'].update({'weather_report':weather})
				## twithlight is updated only once for now 
				if 'twilight' not in locals():
					logger.info("Refreshing twilight.")
					home['environment'].update({'twilight':utils.get_twilight(config['city'])})
					twilight = True
				## localtime
				logger.info("Refreshing localtime.")
				localtime = datetime.now(pytz.timezone(config['timezone']))
				home['environment'].update({'localtime':localtime.strftime('%Y-%m-%d %H:%M:%S')})
				### time of the day 
				logger.info("Refreshing time of day.")
				home['environment'].update({'time_of_day':utils.get_tod(localtime)})
				logger.info("Refreshing Hue lights status.")
				hue_lights = hue.lights
				hue_lights_object = hue.get_light_objects('id')
				# Get or refresh the light names and groups at 360 secs intervals
				for light in hue_lights:
					logger.info("Detected Hue light '%s'..." % light.name)
				groups = hue.get_group()
				for group in groups:
					# groups share names with sublocations so group = sublocation
					sublocation = hue.get_group(int(group), 'name')
					if sublocation in config['outdoor_locations']:
						inout = 'outdoors'
					else:
						inout = 'indoors'
					logger.info("Detected Hue group '%s'" % sublocation)
					lights = hue.get_group(int(group), 'lights')
					if not lights:
						continue
					logger.info("Detected Hue light '%s' in this group" % str(lights))
					items = {}
					for i in lights:
						if runmode == 'debug' and submode == 'hue':
							for key, value in groups.iteritems():
								if groups[key]['name'] == sublocation:
									status = groups[key]
							items.update({int(i):status})
						else:
							status = hue.get_light(int(i), 'on')
							items.update({int(i):status})
					home[inout]['locations'][sublocation]['hue_lights'] = items
				logger.info("Refreshing Presence Scores of all locations.")
				# Check presence status for all locations and refresh presence scores
				now = datetime.now(pytz.timezone(config['timezone'])).replace(tzinfo=None)
				for sublocation, value in home[inout]['locations'].iteritems():
					pa = value['presence']['asserted']
					epa = value['presence']['extended']['asserted']
					ps = value['presence']['score']
					eps = value['presence']['extended']['score']
					pts = datetime.strptime(value['presence']['timestamp'], '%Y-%m-%d %H:%M:%S')
					epts = datetime.strptime(value['presence']['extended']['timestamp'], '%Y-%m-%d %H:%M:%S')
					tsa = now - pts
					etsa = now - epts
					if epa is False:
						if tsa.seconds > config['tsa'] and pa is True:
							nps = ps - 30
							print "Decreasing presence score for location '%s' from '%s' to '%s'." % (sublocation, ps, nps)
							home[inout]['locations'][sublocation]['presence']['score'] = nps
							# if new presence score is below 60 we update the location presence status and push the event to the decider
							if nps < 60:
								home[inout]['locations'][sublocation]['presence']['asserted'] = False
								event = {'presence':{'sublocation':sublocation,'asserted':False,'deviceid':'null'}}
								thread_name = str(sublocation+"_presence")
								decider  = the_decider(name=thread_name, args=(1,), kwargs=event)
								decider.start()
						elif pa is False and ((ps % 10 == 0) and (ps > 0)):
							home[inout]['locations'][sublocation]['presence']['score'] = ps - 10
						elif (eps % 20 == 0) and (eps > 100):
							home[inout]['locations'][sublocation]['presence']['extended']['score'] = eps - 100
						elif eps <= 100:
							home[inout]['locations'][sublocation]['presence']['extended']['score'] = 0
					elif epa is True and etsa.seconds > config['etsa']:
						ncps = eps - 100
						home[inout]['locations'][sublocation]['presence']['extended']['score'] = ncps
						print "Decreasing extended presence score for location '%s' from '%s' to '%s'." % (sublocation, eps, ncps)
						if ncps < 400:
							print("Extended presence status for location '%s' has now expired." % sublocation)
							home[inout]['locations'][sublocation]['presence']['extended']['asserted'] = False
				#home[inout]['locations'][sublocation]['presence']['last'] = self.localtime.strftime('%Y-%m-%d %H:%M:%S')
				# Adjust, activate or deactivate lights where presence is asserted and light conditions have changed.
				for sublocation, value in home[inout]['locations'].iteritems():
					ambient_light = value['ambient_light']
					presence = value['presence']['asserted']
					tod = home['environment']['time_of_day']
					logger.debug("Ambient light in location '%s' is '%s' while presence is '%s' checking if lights are required." % (sublocation, ambient_light, presence))
					for key, value in home[inout]['locations'][sublocation].iteritems():
						# Check if hue lights are available there and what their status are
						if key == 'hue_lights' and config['hue_transitions'] == 'auto':
							for light in value:
								status = home[inout]['locations'][sublocation]['hue_lights'][int(light)]
								# Turn off lights in location if ambient light is medium or high
								if status and ambient_light in ['medium', 'high'] and tod not in ['midnight', 'night']:
									logger.debug("Light '%s' is on and light conditions are '%s'. Turning it off." % (light, ambient_light))
									command =  {'transitiontime' : 10, 'on' : False}
									home[inout]['locations'][sublocation]['hue_lights'][int(light)] = False
									hue.set_light(value, command)
								# Dimm lights down when they are already on and we are approaching midnight
								elif status and tod == 'night' and presence:
									bright = 127
									xy = [0.5,0.4]
									command = {'transitiontime' : 10, 'on' : True, 'bri' : bright, 'xy': xy}
									hue.set_light(value, command)
								elif status and not presence:
									command =  {'transitiontime' : 10, 'on' : False}
									home[inout]['locations'][sublocation]['hue_lights'][int(light)] = False
									hue.set_light(value, command)
									
				time.sleep(self.interval)
			except Exception:
				sys.excepthook(*sys.exc_info())

# MQTT funcions here
def on_connect(client, userdata, flags, rc):
	# Subscribing in on_connect() means that if we lose the connection and
	# reconnect then subscriptions will be renewed.
	if rc == 0:
		logger.warning("Connected to MQTT broker with result code '%s'" % rc)
		# Start Subscriptions here
		client.subscribe(config['mqtt_topic'], 1)
		logger.warning("Subscribed to topic '%s'" % config['mqtt_topic'])
	else:
		logger.warning("Connection to MQTT broker failed. Result Code with '%s'" % rc)

def on_publish(client, userdata, mid):
	logger.debug("PUBLISH '%s' completed." % mid)

def on_message(client, userdata, msg):
	# Take a peek at all messages published into IoT topic and look for messages containing PIR, MIC or VIB events 
	if msg.topic == 'iot/':
		logger.info("Processing Publish '%s'." % msg.payload)
		# convert payload to a dictionary 
		# we should measure the lenght of the payload, count items and check if we counted them all before passing to THE DECIDER 
		payload = ast.literal_eval(msg.payload)
		for key, value in payload.iteritems():
			sublocation = 'null'
			deviceid = 'null'
			if key in ['pir','mic','vib'] and 'event' not in locals():
				event = {}
				# we've got something, now we need to inspect the whole message again
				for key, value in payload.iteritems():
					if key in ['pir','mic','vib']:
						event.update({'sensor':key})
					elif key == 'sublocation':
						event.update({'sublocation':value})
					elif key == 'deviceid':
						event.update({'deviceid':value})
					else:
						continue
				# now we push the message to the learner 
				logger.info("Received an MQTT message with the following '%s', pushing message to the learner for further processing..." % event)
				learner = the_learner('presence', kwargs=event)
				learner.start()
				# get name of running threads, if we are already processing an event for this location we dont push it to the decider
				thread_name = str(event['sublocation'])+"_motion"
				running_threads = get_threads()
				#if thread_name in running_threads and thread_name != 'porch_motion':
				if thread_name in running_threads:
					logger.info("A thread for this event is still running: '%s'. Refreshing timers." % thread_name)
					#add_time(thread_name)
				else:
					#push this event to the decider
					logger.info("Received an MQTT message with the following '%s', pushing message to the decider for further processing..." % event)
					decider = the_decider(name=thread_name, args=(1,), kwargs=event)
					decider.start()
			elif key == 'gpios' and 'thing' not in locals():
				thing = {}
				# we've got something, now we need to inspect the whole message again
				for key, value in payload.iteritems():
					if key == 'sublocation':
						thing.update({'sublocation':value})
					elif key == 'deviceid':
						thing.update({'deviceid':value})
					elif key == 'gpios':
						thing.update({'gpios':value})
					else:
						continue
				# there's no other thread push this message to the learner 
				logger.info("Received an MQTT message with the following '%s', pushing message to the learner for evaluation.." % thing)
				learner = the_learner('thing', kwargs=thing)
				learner.start()
			elif key in ['c','color_temp','light', 'uv', 'uvi', 'lux', 'ir', 'vis'] and 'illuminance' not in locals():
				illuminance = {}
				sensors = {}
				# we received MQTT containing enviromental light information, now we need to inspect the whole message again
				for key, value in payload.iteritems():
					if key == 'sublocation':
						illuminance.update({'sublocation':value})
					elif key == 'deviceid':
						illuminance.update({'deviceid':value})
					elif key in ['c','color_temp','light', 'uv', 'uvi', 'lux', 'ir', 'vis']:
						sensors.update({key:value})
					else:
						continue
				illuminance.update({'illuminance':sensors})
				# now push this message to the learner 
				logger.info("Received an MQTT message with the following ambient light information '%s', pushing message to the learner for evaluation..." % illuminance)
				learner = the_learner('environment', kwargs=illuminance)
				learner.start()
			elif key in ['temperature', 'humidity', 'rain_volt', 'pressure'] and 'climate' not in locals():
				climate = {}
				sensors = {}
				# we received MQTT containing enviromental light information, now we need to inspect the whole message again
				for key, value in payload.iteritems():
					if key == 'sublocation':
						climate.update({'sublocation':value})
					elif key == 'deviceid':
						climate.update({'deviceid':value})
					elif key in ['temperature', 'humidity', 'rain_volt', 'pressure']:
						sensors.update({key:value})
					else:
						continue
				climate.update({'climate':sensors})
				# now push this message to the learner 
				logger.info("Received an MQTT message with the following climate information '%s', pushing message to the learner for evaluation..." % climate)
				learner = the_learner('environment', kwargs=climate)
				learner.start()
			elif key in ['co2', 'dust'] and 'air' not in locals():
				air = {}
				sensors = {}
				# we received MQTT containing enviromental light information, now we need to inspect the whole message again
				for key, value in payload.iteritems():
					if key == 'sublocation':
						air.update({'sublocation':value})
					elif key == 'deviceid':
						air.update({'deviceid':value})
					elif key in ['co2', 'dust']:
						sensors.update({key:value})
					else:
						continue
				air.update({'air':sensors})
				# now push this message to the learner 
				logger.info("Received an MQTT message with the following climate information '%s', pushing message to the learner for evaluation..." % air)
				learner = the_learner('environment', kwargs=air)
				learner.start()
			else:
				continue
def on_disconnect(client, userdata, rc):
	if rc != 0:
		logger.warning("Unexpected MQTT Broker disconnection. Will auto-reconnect.")

def on_log(mosq, obj, level, string):
	logger.debug(string)

class write_stats(threading.Thread):
	def __init__(self, interval):
		threading.Thread.__init__(self)
		self.interval = interval
		self.name = 'write_stats'
		return
	def run(self):
		import os_stats, resource
		logger.info("Writing OS stats to logs at '%s' secs intervals..." % self.interval)
		while True:
			try:
				cpu = os_stats.get_usage('cpu')
				mem_used, mem_per = os_stats.get_usage('mem')
				disk_used, disk_percent = os_stats.get_usage('disk')
				up = os_stats.get_usage('uptime')
				tx, rx = os_stats.get_usage('network')
				total_threads = threading.activeCount()
				with open('stats.txt', 'w') as outfile:
					json.dump(home, outfile)
				logger.warning("OS stats: Uptime:%s CPU:%s Mem:%s Disk:%s TX:%s RX:%s" % (up, cpu, mem_used, disk_used, tx, rx))
				logger.warning("App stats: Memory usage: %s (kb) Runtime: %s" % (resource.getrusage(resource.RUSAGE_SELF).ru_maxrss, datetime.now()-start_stats))
				time.sleep(self.interval)
			except Exception:
				sys.excepthook(*sys.exc_info())

def check_network():
	import socket
	loop = 1
	while loop == 1:
		try:
			broker = socket.gethostbyname(config['mqtt_broker_hostname'])
			hue_bridge = socket.gethostbyname(config['hue_bridge_hostname'])
 			b = socket.create_connection((broker, config['mqtt_broker_port']), 2)
 			h = socket.create_connection((hue_bridge, config['hue_bridge_port']), 2)
			logger.warning("Network checks passed!, both '%s' and '%s' are reachable." % (config['mqtt_broker_hostname'], config['hue_bridge_hostname']))
			loop = 0
		except Exception:
			logger.warning("Network checks failed!, stalling until MQTT Broker '%s' and/or '%s' Hue Bridge become reachable..." % (config['mqtt_broker_hostname'], config['hue_bridge_hostname']))
			time.sleep(5)

class hue_thread(threading.Thread):
	def __init__(self, bridge):
		threading.Thread.__init__(self)
		self.hue_bridge_hostname = bridge
		self.name = 'hue_thread'
		return
	def run(self):
		try:
			from phue import Bridge
			global hue_lights
			global hue
			global hue_lights_object
			logger.warning("Connecting to Hue bridge '%s' " % self.hue_bridge_hostname)
			hue = Bridge(self.hue_bridge_hostname)
			hue.connect()
			hue.get_api()
			hue_lights = hue.lights
			hue_lights_object = hue.get_light_objects('id')
		except Exception:
			sys.excepthook(*sys.exc_info())

class mqtt_thread(threading.Thread):
	def __init__(self):
		threading.Thread.__init__(self)
		self.name = 'mqtt_thread'
		return
	def run(self):
		try:
			global mqttc
			mqttc = mqtt.Client(config['hostid'])
			mqttc.username_pw_set(config['mqtt_broker_username'], config['mqtt_broker_pass'])
			mqttc.connect(config['mqtt_broker_hostname'], config['mqtt_broker_port'], 80)
			mqttc.on_connect = on_connect
			mqttc.on_message = on_message
			mqttc.on_publish = on_publish
			mqttc.on_disconnect = on_disconnect
			# Uncomment here to enable MQTT debug messages
			#mqttc.on_log = on_log
			mqttc.loop_start()
		except Exception:
			sys.excepthook(*sys.exc_info())

class HTTPRequestHandler(BaseHTTPRequestHandler):
	def do_GET(self):
		self.send_response(200)
		self.send_header('Content-Type', 'text/html')
		self.end_headers()
		self.table_attributes = {}
		http_json = json.dumps(home).replace("false", "0").replace("true", "1")
		self.wfile.write(json2html.convert(json = http_json))
		#else:
		#	self.send_response(403)
		#	self.send_header('Content-Type', 'application/json')
		#	self.end_headers()
		return
    
class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
	allow_reuse_address = True
 
	def shutdown(self):
		self.socket.close()
		HTTPServer.shutdown(self)
    
class http_server():
	def __init__(self, ip, port):
		self.server = ThreadedHTTPServer((ip, port), HTTPRequestHandler)
 
	def start(self):
		self.server_thread = threading.Thread(target=self.server.serve_forever)
		self.server_thread.daemon = True
		self.server_thread.start()
 
	def waitForThread(self):
		self.server_thread.join()
 
	def stop(self):
		self.server.shutdown()
		self.waitForThread()

def activate(gpio, mode, sublocation, deviceid):
	if gpio in ['buz', 'led']:
		if mode == 'warn':
			for i in range(5):
				#mqttc.publish('dispatcher/home/%s/%s/%s' % (sublocation, deviceid, gpio), 'on')
				mqttc.publish('dispatcher', payload=json.dumps({'deviceid':deviceid, 'gpio':gpio, 'status':'on'}))
				time.sleep(0.7)
				#mqttc.publish('dispatcher/home/%s/%s/%s' % (sublocation, deviceid, gpio), 'off')
				mqttc.publish('dispatcher', payload=json.dumps({'deviceid':deviceid, 'gpio':gpio, 'status':'off'}))
				time.sleep(0.05)
		elif mode == 'midnight':
			for i in range(5):
				#mqttc.publish('dispatcher/home/%s/%s/%s' % (sublocation, deviceid, gpio), 'on')
				mqttc.publish('dispatcher', payload=json.dumps({'deviceid':deviceid, 'gpio':gpio, 'status':'on'}))
				time.sleep(1.4)
				#mqttc.publish('dispatcher/home/%s/%s/%s' % (sublocation, deviceid, gpio), 'off')
				mqttc.publish('dispatcher', payload=json.dumps({'deviceid':deviceid, 'gpio':gpio, 'status':'off'}))
				time.sleep(0.5)
		else:
			#mqttc.publish('dispatcher/home/%s/%s/%s' % (sublocation, deviceid, gpio), 'on')
			mqttc.publish('dispatcher', payload=json.dumps({'deviceid':deviceid, 'gpio':gpio, 'status':'on'}))
			if mode == 'feedback':
				time.sleep(0.0004)
			elif mode == 'porch':
				time.sleep(0.15)
			elif mode == 'blink':
				time.sleep(0.3)
			#mqttc.publish('dispatcher/home/%s/%s/%s' % (sublocation, deviceid, gpio), 'off')
			mqttc.publish('dispatcher', payload=json.dumps({'deviceid':deviceid, 'gpio':gpio, 'status':'off'}))
			time.sleep(0.0004)
			mqttc.publish('dispatcher', payload=json.dumps({'deviceid':deviceid, 'gpio':gpio, 'status':'off'}))
	elif gpio == 'relay':
		if mode == 'warn':
			for i in range(5):
				#mqttc.publish('dispatcher/home/%s/%s/%s' % (sublocation, deviceid, gpio), 'on')
				mqttc.publish('dispatcher', payload=json.dumps({'deviceid':deviceid, 'gpio':gpio, 'status':'on'}))
				time.sleep(1)
				#mqttc.publish('dispatcher/home/%s/%s/%s' % (sublocation, deviceid, gpio), 'off')
				mqttc.publish('dispatcher', payload=json.dumps({'deviceid':deviceid, 'gpio':gpio, 'status':'off'}))
				time.sleep(0.4)
		elif mode == 'on':
			#mqttc.publish('dispatcher/home/%s/%s/%s' % (sublocation, deviceid, gpio), 'on')
			mqttc.publish('dispatcher', payload=json.dumps({'deviceid':deviceid, 'gpio':gpio, 'status':'on'}))
		elif mode == 'off':
			#mqttc.publish('dispatcher/home/%s/%s/%s' % (sublocation, deviceid, gpio), 'off')
			mqttc.publish('dispatcher', payload=json.dumps({'deviceid':deviceid, 'gpio':gpio, 'status':'off'}))
		
def get_threads():
	threads = []
	main_thread = threading.current_thread()
	for t in threading.enumerate():
		if t is main_thread:
			continue
		threads.append(t.getName())
	return threads

if __name__ == "__main__":
	print "Home Assistant HA is spinning. Check /var/log/ha.log for more details..."
	# Load config from file config.cfg
	load_config()
	# Perfrom network checks and wait there until online
	check_network()
	# Load enviromental variables
	load_environment()
	# Customized exception handling
	sys.excepthook = utils.my_excepthook
	try:
		# Call thread to handle MQTT 
		thread0 = mqtt_thread()
		thread0.daemon = True
		thread0.start()
		logger.warning("Started mqtt thread...")
		# Thread hue bridge here
		thread1 = hue_thread(config['hue_bridge_hostname'])
		thread1.daemon = True
		thread1.start()
		logger.warning("Started hue thread...")
		# Write app stats to log
		thread2 = write_stats(config['write_stats_interval'])
		thread2.daemon = True
		thread2.start()
		logger.warning("Started write_stats thread...")
		# Supervisor thread
		thread3 = the_supervisor(config['supervisor_interval'])
		thread3.daemon = True
		thread3.start()
		logger.warning("Started the_supervisor thread...")
		# Web server thread here
		#thread4 = HTTPServer(('', int(config['http_port'])), http_server)
		#thread4.serve_forever()
		thread4 = http_server(config['http_ip'], config['http_port'])
		thread4.start()
		#thread4.waitForThread()
		logger.warning("Started http_server thread...")
		if runmode == 'debug':
			while True:
				print "Home Status:"
				print json.dumps(home, sort_keys=True, indent=4)
				print ""
				print "Number of active threads: %s" % threading.active_count()
				time.sleep(30.0 - ((time.time() - starttime) % 30.0))

	except KeyboardInterrupt:
		logging.exception("Got exception on main handler:")
		elogger.exception("Got exception on main handler:")
		mqttc.loop_stop()
		mqttc.disconnect()
		thread4.stop()
		raise

	except:
		logging.exception("Other error or exception occurred!")
		elogger.exception("Other error or exception occurred!")
		mqttc.loop_stop()
		mqttc.disconnect()
		thread4.stop()

	while True:
		time.sleep(0.1)
		pass
