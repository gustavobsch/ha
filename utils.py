#!/usr/bin/python
import subprocess, logging
logger = logging.getLogger('the_decider')
elogger = logging.getLogger('email')
logger.info("Library os_stats.py started...")

### Customized exception handler
def my_excepthook(excType, excValue, traceback, logger=logger):
	#import subprocess
	if excType != KeyboardInterrupt:
		d = {'hostname': 'the_decider'}
		logger.error("Logging an uncaught exception", exc_info=(excType, excValue, traceback))
		elogger.critical("Logging an uncaught exception", extra=d, exc_info=(excType, excValue, traceback))
		#logger.warning("Restarting application after crash %s..." % excType)
		#subprocess.check_output(["systemctl", "restart", "publisher.service"])
	elif excType != KeyboardInterrupt and runmode == 'test':
		logger.warning("Unclean exit caused by a code crash while running in test mode'%s'" % excType)
	elif excType == KeyboardInterrupt:
		logger.warning("Caught ^C, exiting gracefully...")

def get_weather(city, appid):
	import json, requests
	try:
		url = "http://api.openweathermap.org/data/2.5/weather?id=%s&APPID=%s&units=metric" % (city, appid)
		request = requests.get(url)
		json = json.loads(request.text)
		if json['wind']:
			weather = {
			'weather':{'main':json['weather'][0]['main'], 'description':json['weather'][0]['description']},
			'main':{'pressure':json['main']['pressure'], 'temp':json['main']['temp'], 'humidity':json['main']['humidity']},
			'wind':{'speed':json['wind']['speed'], 'deg':json['wind']['deg']}}
		else:
			weather = {
			'weather':{'main':json['weather'][0]['main'], 'description':json['weather'][0]['description']},
			'main':{'pressure':json['main']['pressure'], 'temp':json['main']['temp'], 'himidity':json['main']['humidity']}}
		return weather
	except:  # catch *all* exceptions and return empty dictionary
		weather = {}
		return weather

def get_twilight(city):
	import pytz, time
	from datetime import datetime
	from astral import Astral
	from datetime import datetime, timedelta
	a = Astral()
	a.solar_depression = 'civil'
	city = a['Santiago']
	now = datetime.now(pytz.utc)
	sun = city.sun(date=now, local=True)
	#print('Dawn:    %s' % str(sun['dawn']))
	#print('Sunrise: %s' % str(sun['sunrise']))
	#print('Noon:    %s' % str(sun['noon']))
	#print('Sunset:  %s' % str(sun['sunset']))
	#print('Dusk:    %s' % str(sun['dusk']))
	if now >= sun['dawn'] and now <= sun['sunrise']:
		return 'dawn'
	elif now >= sun['sunrise'] and now <= sun['noon']:
		return 'sunrise'
	elif now >= sun['noon'] and now <= sun['sunset']:
		return 'noon'
	elif now >= sun['sunset'] and now <= sun['dusk']:
		return 'sunset'
	elif now >= sun['dusk'] or now <= sun['dawn']:
		return 'night'

def get_tod(time):
	if time.hour >= 6 and time.hour < 12:
		return "morning"
	elif time.hour >= 12 and time.hour < 18:
		return "afternoon"
	elif time.hour >= 18 and time.hour < 22:
		return "evening"
	elif time.hour >= 22 and time.hour < 24:
		return "night"
	elif time.hour >= 0 and time.hour < 6:
		return "midnight"

class ringlist:
	def __init__(self, length):
		self.__data__ = []
		self.__full__ = 0
		self.__max__ = length
		self.__cur__ = 0

	def append(self, x):
		if self.__full__ == 1:
			for i in range (0, self.__cur__ - 1):
				self.__data__[i] = self.__data__[i + 1]
			self.__data__[self.__cur__ - 1] = x
		else:
			self.__data__.append(x)
			self.__cur__ += 1
			if self.__cur__ == self.__max__:
				self.__full__ = 1

	def get(self):
		return self.__data__

	def remove(self):
		if (self.__cur__ > 0):
			del self.__data__[self.__cur__ - 1]
			self.__cur__ -= 1

	def size(self):
		return self.__cur__

	def maxsize(self):
		return self.__max__

	def __str__(self):
		return ''.join(self.__data__)



